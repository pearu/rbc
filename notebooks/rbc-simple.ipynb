{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Remote Backend Compiler - RBC\n",
    "\n",
    "In RBC, a function compilation process is split between the local host (client) and remote host (server).\n",
    "In client, the functions are compiled to LLVM IR string. The IR is sent to the server where it is compiled to machine code.\n",
    "\n",
    "When client calls the function, the arguments are sent to the server, the server executes the function call, and the results will be sent back to client as a response.\n",
    "\n",
    "To use RBC, import the `rbc` package and define remote JIT decorator. The remote JIT decorator has three use cases:\n",
    "\n",
    "1. decorate Python functions that implementation will be used as a template for low-level functions\n",
    "2. define signatures of the low-level functions\n",
    "3. start/stop remote JIT server"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import rbc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Remote JIT decorator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "rjit = rbc.RemoteJIT(host='localhost')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can start the server from a separate process as well as in background of the current process:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "staring rpc.thrift server: /home/pearu/git/plures/rbc/rbc/remotejit.thrift"
     ]
    }
   ],
   "source": [
    "rjit.start_server(background=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The server will be stopped at the end of this notebook, see below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use `rjit` as decorator with signature specifications\n",
    "\n",
    "A function signature can be \n",
    "- in the form of a string, e.g. `\"int64(int64, int64)\"`, \n",
    "- or in the form of a numba function signature, e.g. `numba.int64(numba.int64, numba.int64)`,\n",
    "- or a `ctypes.CFUNCTYPE` instance, e.g. `ctypes.CFUNCTYPE(ctypes.c_int64, ctypes.c_int64, ctypes.c_int64)`.\n",
    "\n",
    "If a function uses annotations, these are also used for determining the signature of a function.\n",
    "\n",
    "For instance, the following example will define an `add` function for arguments with `int` or `float` type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "@rjit('f64(f64,f64)')\n",
    "def add(a: int, b: int) -> int:\n",
    "    return a + b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "float64(float64, float64)\n",
      "int64(int64, int64)\n"
     ]
    }
   ],
   "source": [
    "print('\\n'.join(map(str, add._signatures)))  # to view the currently defined signatures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define a target\n",
    "\n",
    "To set a target for LLVM IR, use the `target` method of the function object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "prev_target = add.target('host')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Currently supported targets are: `\"host\"`. TODO: `\"cuda\"`, `\"cuda32\"`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Try it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "add(1, 2)      # int inputs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3.5"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "add(1.5, 2.0)  # float inputs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "could not find matching function to given argument types: `complex128, int64`\n"
     ]
    }
   ],
   "source": [
    "try:             # complex inputs\n",
    "    add(1j, 2)   # expect a failure\n",
    "except Exception as msg:\n",
    "    print(msg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add support for complex inputs:\n",
    "add.add_signature('complex128(complex128, complex128)')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2+1j)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "add(1j, 2)  # now it works"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Debugging\n",
    "\n",
    "For debugging, one can view the generated LLVM IR using the `get_IR` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "; ModuleID = 'add'\n",
      "source_filename = \"<ipython-input-4-47cad48366e9>\"\n",
      "target datalayout = \"e-m:e-i64:64-f80:128-n8:16:32:64-S128\"\n",
      "target triple = \"x86_64-unknown-linux-gnu\"\n",
      "\n",
      "@\"_ZN08NumbaEnv8__main__7add$244Edd\" = common local_unnamed_addr global i8* null\n",
      "@\"_ZN08NumbaEnv8__main__7add$245Exx\" = common local_unnamed_addr global i8* null\n",
      "@\"_ZN08NumbaEnv8__main__7add$246E10complex12810complex128\" = common local_unnamed_addr global i8* null\n",
      "\n",
      "; Function Attrs: norecurse nounwind\n",
      "define i32 @\"_ZN8__main__7add$244Edd\"(double* noalias nocapture %retptr, { i8*, i32 }** noalias nocapture readnone %excinfo, double %arg.a, double %arg.b) local_unnamed_addr #0 {\n",
      "entry:\n",
      "  %.16 = fadd double %arg.a, %arg.b\n",
      "  store double %.16, double* %retptr, align 8\n",
      "  ret i32 0\n",
      "}\n",
      "\n",
      "; Function Attrs: norecurse nounwind readnone\n",
      "define double @add_daddA(double %.1, double %.2) local_unnamed_addr #1 {\n",
      "entry:\n",
      "  %.16.i = fadd double %.1, %.2\n",
      "  ret double %.16.i\n",
      "}\n",
      "\n",
      "; Function Attrs: norecurse nounwind\n",
      "define i32 @\"_ZN8__main__7add$245Exx\"(i64* noalias nocapture %retptr, { i8*, i32 }** noalias nocapture readnone %excinfo, i64 %arg.a, i64 %arg.b) local_unnamed_addr #0 {\n",
      "entry:\n",
      "  %.16 = add nsw i64 %arg.b, %arg.a\n",
      "  store i64 %.16, i64* %retptr, align 8\n",
      "  ret i32 0\n",
      "}\n",
      "\n",
      "; Function Attrs: norecurse nounwind readnone\n",
      "define i64 @add_lallA(i64 %.1, i64 %.2) local_unnamed_addr #1 {\n",
      "entry:\n",
      "  %.16.i = add nsw i64 %.2, %.1\n",
      "  ret i64 %.16.i\n",
      "}\n",
      "\n",
      "; Function Attrs: norecurse nounwind\n",
      "define i32 @\"_ZN8__main__7add$246E10complex12810complex128\"({ double, double }* noalias nocapture %retptr, { i8*, i32 }** noalias nocapture readnone %excinfo, double %arg.a.0, double %arg.a.1, double %arg.b.0, double %arg.b.1) local_unnamed_addr #0 {\n",
      "entry:\n",
      "  %.37 = fadd double %arg.a.0, %arg.b.0\n",
      "  %.40 = fadd double %arg.a.1, %arg.b.1\n",
      "  %retptr.repack = getelementptr inbounds { double, double }, { double, double }* %retptr, i64 0, i32 0\n",
      "  store double %.37, double* %retptr.repack, align 8\n",
      "  %retptr.repack1 = getelementptr inbounds { double, double }, { double, double }* %retptr, i64 0, i32 1\n",
      "  store double %.40, double* %retptr.repack1, align 8\n",
      "  ret i32 0\n",
      "}\n",
      "\n",
      "; Function Attrs: norecurse nounwind readnone\n",
      "define { double, double } @add_DaDDA({ double, double } %.1, { double, double } %.2) local_unnamed_addr #1 {\n",
      "entry:\n",
      "  %extracted.real = extractvalue { double, double } %.1, 0\n",
      "  %extracted.imag = extractvalue { double, double } %.1, 1\n",
      "  %extracted.real.1 = extractvalue { double, double } %.2, 0\n",
      "  %extracted.imag.1 = extractvalue { double, double } %.2, 1\n",
      "  %.37.i = fadd double %extracted.real, %extracted.real.1\n",
      "  %.40.i = fadd double %extracted.imag, %extracted.imag.1\n",
      "  %.16.fca.0.insert = insertvalue { double, double } undef, double %.37.i, 0\n",
      "  %.16.fca.1.insert = insertvalue { double, double } %.16.fca.0.insert, double %.40.i, 1\n",
      "  ret { double, double } %.16.fca.1.insert\n",
      "}\n",
      "\n",
      "attributes #0 = { norecurse nounwind }\n",
      "attributes #1 = { norecurse nounwind readnone }\n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(add.get_IR())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stopping the RBC server"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "... stopping rpc.thrift server\n"
     ]
    }
   ],
   "source": [
    "rjit.stop_server()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Environment (conda_remote-numba-dev)",
   "language": "python",
   "name": "conda_remote-numba-dev"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
