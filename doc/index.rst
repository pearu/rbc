.. meta::
   :robots: index, follow
   :description: rbc documentation
   :keywords: rbc, remote, compiler, jit

.. sectionauthor:: Pearu Peterson <pearu.peterson at quansight.com>


Remote Backend Compiler
=======================

The RBC project provides a JIT compiler with remote backend.


rbc
---

A Python package implementing a remote backend compiler using numba
and llvmlite tools.

.. toctree::
   :maxdepth: 1

   rbc/index.rst


Releases
--------

.. toctree::
   :maxdepth: 1

   releases/index.rst
