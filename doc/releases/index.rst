========
Releases
========

v0.1.0dev2 (September ?, 2019)
------------------------------

- Add OmnisciDB UDF array arguments support.
- Created conda-forge package rbc.
- Created pip package rbc-project.
- Renamed irtools.compile_to_IR to irtools.compile_to_LLVM, returns
  llvmlite.binding.ModuleRef instance.


v0.1.0dev1 (August 2, 2019)
---------------------------

First release.
